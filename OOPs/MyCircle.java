class MyCircle{
	private int radius;
	public MyCircle(){
		System.out.println("Object has been created");
	}
	public MyCircle(int r){
		this.radius = r;
		System.out.println("Parametrised constructor called");
	}

	public void area(){
		System.out.println("Area of Circle is: "+3.14*this.radius*this.radius); 
	}
	public void circum(){
		System.out.println("Circumference of circle is:"+2*3.14*this.radius*this.radius);
	}
	
	@Override
	public String toString(){
		return "Current status of radius is: "+this.radius;
	}

}

class MainClass{
	public static void main(String[] arg){
		MyCircle m1 =  new MyCircle();
		MyCircle m2 =  new MyCircle(5);

		m1.area();
		m2.area();
		m1.circum();
		m2.circum();
		System.out.println(m1);
		System.out.println(m2);
		
	}
}
