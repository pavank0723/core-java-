/*
 Design a class Rectangle
 data members:
 length
 breadth
 
 member function / method:
 setDimension()
 area()
 perimeter()
*/

import java.util.Scanner;

class Rectangle1{
		Scanner sc = new Scanner(System.in);

		int length, breadth;

		public void setDimension(int d,int e){
			this.length = d;
			this.breadth = e;
		}

		/*public int getDimension(){
			return this.length + this.breadth;
		}*/

		public void area(){
			int a = this.length*this.breadth;
			System.out.println("Area is: "+a);
		}

		public void perimeter(){
			int p = 2*(this.length + this.breadth);
			System.out.println("Perimeter is: "+p);
		}
}

class MainRect1{
	public static void main(String[] arg){
		Rectangle1 r = new Rectangle1();
		r.setDimension(4,6);
		r.area();
		r.perimeter();
	}
}
