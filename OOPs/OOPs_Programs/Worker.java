/*
   Design a class Worker
   data members:
    wages
    wdays
   member function / method:
    setData()
    payment()
*/

import java.util.Scanner;

class Worker{
	public int wages, wdays;

	public void setData(int a, int d){
		this.wages = a;
		this.wdays = d;

	}

	public void payment(){

		System.out.println("Payment of the worker is: ");
	}
}
