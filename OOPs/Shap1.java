
import java.util.Scanner;
class Shape{

        public void area(){
                System.out.println("This is the AREA METHOD");
        }

        public Shape(){
                System.out.println("This is the Shape constuctor");
        }
}

class Circle extends Shape{
        int radius;

        public Circle(int r){
                this.radius = r;
                System.out.println("Circle Drawn...");
        }

        public void area(){
                System.out.println("AREA METHOD Circle: "+this.radius);
        }
}

class Rectangle extends Shape{
        int length, breadth;

        public Rectangle(int l, int b){
                this.length = l;
                this.breadth = b;
                System.out.println("Rectangle Drawn...");

        }
        public void area(){
                System.out.println("AREA METHOD Rectangle: Length "+this.length+" Breadth "+this.breadth);
		int area = length*breadth;
		System.out.println("Area of Rectangle: "+area);
		
        }
}

class MainShape1{
        public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int n = sc.nextInt();
                Shape s;

		switch(n){
			case 1:
				s = new Circle(4);
				break;
			case 2:
				s = new Rectangle(5,7);
				break;
			default:
                		s = new Shape();

		}
		s.area();
       
	}
}
