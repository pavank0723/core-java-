
class Overload{
	int sum = 0;
	public void add(int a){
		System.out.println("Add1: "+a);
		sum +=a;
		System.out.println(sum);
	}

	public void add(int a, int b){
		System.out.println("Add2: num1-> "+a+" num2-> "+b);
                sum = a+b;
		System.out.println(sum);
	}

	public void add(int a, int b, int c){
		System.out.println("Add3: num1 -> "+a+" num2 ->"+b+" num3 ->"+c);
		sum = a+b+c;
                System.out.println(sum);
	}

	public static void main(String[] arg){
		Overload ol = new Overload();
		ol.add(5);
		ol.add(5,6);
		ol.add(5,6,7);
		
	}
}
