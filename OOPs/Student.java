class Student{
 		String name;
		String city;
		int age;

		Student(String n, String c, int a){
			this.name = n;
			this.city = c;
			this.age = a;
			
		}

		void showData(){
			System.out.println("Name: "+name);
			System.out.println("City: "+city);
			System.out.println("Age: "+age);
			
		}
		@Override
		public int hashCode(){
			return this.age+this.name.hashCode()+this.city.hashCode();
		}

		public String toString(){
			return this.name+"\t"+this.city+ "\t"+this.age + "\n";
		}
}
class Main{

		public static void main(String[] arg){
			Student s1 = new Student("Satyanshu","Mumbai",20);
			Student s2 = new Student("Satyansh","Mumbai",20);
			s1.showData();

			System.out.println("Student :"+s1.hashCode());
			System.out.println("Student :"+s2.hashCode());

		}

	}

