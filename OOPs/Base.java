
class Base{

	public Base(){
		System.out.println("This is the Base constuctor");
	}
}

class Derive extends Base{
	public Derive(){
		System.out.println("This is the Derive constructor");
	}
}

class Main{
	public static void main(String[] arg){
		new Base();
		new Derive();
	}
}


