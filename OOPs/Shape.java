
class Shape{

	public void area(){
		System.out.println("This is the AREA METHOD");
	}

	public Shape(){
		System.out.println("This is the Shape constuctor");
	}
}

class Circle extends Shape{
	int radius;

	public Circle(int r){
		this.radius = r;
		System.out.println("Circle Constructor");
	}

	public void area(){
		System.out.println("AREA METHOD Circle: "+this.radius);
	}
}

class Rectangle extends Shape{
	int length, breadth;

	public Rectangle(int l, int b){
		this.length = l;
		this.breadth = b;
		System.out.println("Rectangle constructor");

	}
	public void area(){
		System.out.println("AREA METHOD Rectangle: Length "+this.length+" Breadth "+this.breadth);
	}
}

class Main{
	public static void main(String[] arg){
		Circle c = new Circle(7);
		c.area();
		Rectangle r = new Rectangle(5,6);
		r.area();

	}
}


