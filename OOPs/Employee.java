class Employee{
	private String name, city;
	private float salary;
	private int age;

       public Employee(){
		System.out.println("Default constuctor called");
	}
       public Employee(String n, String c, float sal, int a){
	       this.name = n;
	       this.city = c;
	       this.salary = sal;
	       this.age = a;
	       
       		System.out.println("Parameterized Constructor called");
       }
       @Override
       public int hashCode(){
	System.out.println("Hash code is called");
       	return this.age + Float.floatToIntBits(this.salary) + this.city.hashCode() + this.name.hashCode();
       }

       @Override
       public boolean equals(Object obj){
	
      	       if(obj instanceof Employee){
		 Employee e = (Employee) obj;
		 if(this.name.equals(e.name) && 
			this.city.equals(e.city) &&
			this.age == e.age &&
			this.salary == e.salary){
				return true;
		}
	}
       	
	/*if(this.hashCode() == obj.hashCode()){
		return true;
	}*/
	return false;
       }


}
class MainClass{
	public static void main(String[] arg){
		Employee e1 =  new Employee("Employee1","Mumbai",23000f,23);
		Employee e2 =  new Employee("Employee1","Mumbai",33000f,23);

		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());
		System.out.println(e1.equals(e2));
		
	}
}
