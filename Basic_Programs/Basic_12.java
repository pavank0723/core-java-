import java.util.Scanner;
class Basic_12{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the 2 number: ");
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();

		System.out.println("-> Before exchange their value: \n");		
		System.out.println("Number 1 is: "+num1);
		System.out.println("Number 2 is: "+num2);

		num1 +=num2;
		num2 = num1 - num2;
		num1 = num1 - num2;

		System.out.println("<- ->After exchange their value: \n");
        System.out.println("Number 1 is: "+num1);
        System.out.println("Number 2 is: "+num2);

	}
}
