import java.util.Scanner;
class Basic_6{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		int sum = 0, rem;
		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		while(num>0){
			rem = num%10;
			sum += rem;
			num /=10;
		}
		System.out.println("Sum of last digit is: "+sum);
	}
}

