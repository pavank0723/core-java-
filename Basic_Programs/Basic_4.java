import java.util.Scanner;

class Basic_4{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		int sum = 0,rem;
		System.out.println("Enter the 4 digits number: ");
		int a = sc.nextInt();

		while(a>0){
			rem = a%10;
			sum = sum + rem;
			a = a/10;
		}
		System.out.println("Sum of 4 digits is: "+sum);
	}
}
