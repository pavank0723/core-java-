import java.util.Scanner;
class Basic_7{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		int rev = 0,rem;

		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		while(num!=0){
			rem = num%10;
			rev = rev*10+rem;
			num /=10;
		}
		System.out.println("Reverse of the number is: "+rev);
	}
}
