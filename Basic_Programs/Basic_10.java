//WAP to read marks of 5 subjects and print total and percentage
import java.util.Scanner;

class Basic_10{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the marks of 5 subjects:");
		System.out.println("Subject1:");
		int sub1 = sc.nextInt();
		
		System.out.println("Subject2:");
		int sub2 = sc.nextInt();
		
		System.out.println("Subject3:");
		int sub3 = sc.nextInt();
		
		System.out.println("Subject4:");
		int sub4 = sc.nextInt();
		
		System.out.println("Subject5:");
		int sub5 = sc.nextInt();
		
		int sum = sub1 + sub2 + sub3 + sub4 + sub5;
		
		float percentage = sum/5;
		
		System.out.println("Percentage: "+percentage+"%");
		
	}
}

