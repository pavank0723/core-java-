/* WAP to print all prime number in range from 1 to 100
   => output: 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
*/

class Loop_8{
	public static void main(String[] arg){

	int temp = 0;

	for(int i=1;i<=100;i++){
 		for(int j =2;j<=i-1;j++){
			if(i%j==0){
				temp = temp + 1;
			}
		}
		if(temp==0){
			System.out.print(i+" ");
		}
		else{
			temp=0;
		}

	}
  }
}

