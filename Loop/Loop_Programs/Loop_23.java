/*
  WAP to find sum of series
  S = 1 + 2! + 3! + …. N!
*/

import java.util.Scanner;

class Loop_23{
	public static void main(String[] arg) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int n = sc.nextInt();
		int f = 1,sum=1;

		System.out.print(f+"!");
		for(int i = 2;i<=n;i++){
			System.out.print(" + ");
			System.out.print(i+"! ");
			f = f * i;
			sum = sum +f;
		}
		System.out.println("\nTotal = "+sum);
	}
}

