/*
  WAP to find sum of series
  S = 1 + 1/2 + 1/3 + 1/4 + 1/5 + …. + 1/N
*/

import java.util.Scanner;

class Loop_24{
	public static void main(String[] arg) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int n = sc.nextInt();
		int f = 1;
		double c=1.0,sum=1.0;

		System.out.print(f);
		for(int i = 2;i<=n;i++){
			System.out.print(" + ");
			System.out.print(f+"/"+i+" ");
			sum = sum + c/i;
		}
		System.out.println("\nTotal = "+sum);
	}
}


