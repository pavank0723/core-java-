/*
   WAP to find sum of series
   S = 1 + 2 + 3 + …. + N 

*/

import java.util.Scanner;

class Loop_22{
	public static void main(String[] arg) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int n = sc.nextInt();
		int s = 1;
		
		System.out.print(s);
		for(int i = 2;i<=n;i++){
			System.out.print(" + ");
			System.out.print(i);
			s = s + i;
		}
		System.out.print("\nTotal = "+s);
	}
}
