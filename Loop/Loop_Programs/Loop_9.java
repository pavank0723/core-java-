/*WAP to print first 10 fibonacci number
   output: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
*/

class Loop_9{
	public static void main(String[] arg){
		
		int a = 0,b = 1;
		System.out.print(a+" "+b);

		int c;
		for(int i = 1;i<=10;i++){
			c = a + b;
			System.out.print(" "+c);
			a = b;
			b = c;
		}
		
	}
}
