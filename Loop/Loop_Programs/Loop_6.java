//WAP to read a number and find reverse of that number

import java.util.Scanner;

class Loop_6{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		int rev = 0,rem;

		while(num!=0){
			rem = num % 10;
			rev = rev*10 + rem;
			num /=10;
		}
		System.out.println("Reverse of that number "+rev);

	}
}

