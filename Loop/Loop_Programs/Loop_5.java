//WAP to read a number and find factorial of that number

import java.util.Scanner;

class Loop_5{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		int i, mul = 1;
		System.out.println("Numbers are ↓");
		System.out.print("1");
		for(i = 2 ;i<=num;i++){
			System.out.print(" * ");
			System.out.print(i);
			mul *= i;
		}		
		System.out.println("\nTotal = "+mul);
	}	
}

