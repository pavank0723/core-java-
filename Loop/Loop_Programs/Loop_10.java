/*WAP to read a number and find sum of its digits
  input: 12345
  output: 1 + 2 + 3 + 4 + 5 = 15
*/

import java.util.Scanner;

class Loop_10{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int temp,sum = 0;
		while(num!=0){
			temp = num%10;
			num/=10;
			sum = temp + sum;
		}
		System.out.println(sum);
	}
}
