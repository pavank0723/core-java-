//WAP to read 2 numbers a and b and print all numbers from a to b

import java.util.Scanner;

class Loop_19{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the 1st number: ");
		int a = sc.nextInt();

		System.out.print("Enter the 2nd number: ");
                int b = sc.nextInt();
		
		System.out.println("All numbers from 1st number to 2nd number ");
		for(int num=a;num<=b;num++){
			System.out.print(num+" ");
		}
	}
}
