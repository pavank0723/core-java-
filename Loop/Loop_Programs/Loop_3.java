//WAP to print all number from 1 to 10 by using while, for and do -while loop

class Loop_3{
	public static void main(String[] arg){

		System.out.println("1 to 10 numbers are");
		System.out.println("→ Using For Loop");
		int i=1;
		for(i=1;i<=10;i++){
			System.out.print(i+" ");
		}
		System.out.println("\n→ Using While Loop");
		int j = 1;
		while(j<=10){		
			System.out.print(j+" ");
			j++;
		}
		System.out.println("\n→ Using Do while Loop");

		int k = 1;
		do{
			System.out.print(k+" ");
			k++;
		}while(k<=10);
		System.out.println("");
	}
}
