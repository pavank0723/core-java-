//WAP to read a number and print all numbers from 1 to that number

import java.util.Scanner;

class Loop_1{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		System.out.println("↓");
		for(int i=1;i<=num;i++){
			System.out.println(i);
		}
	}
}
