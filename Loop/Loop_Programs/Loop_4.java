//WAP to read a number and find sum of all numbers from 1 to that number

import java.util.Scanner;

class Loop_4{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		int i, sum = 1;
		System.out.println("Numbers are ↓");
		System.out.print("1");

		for(i = 2 ;i<=num;i++){
			System.out.print(" + ");
			System.out.print(i);

			sum += i;
		}		
		System.out.println(" = "+sum);
	}	
}
