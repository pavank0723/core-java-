//WAP to read number and print all even numbers from 1 to that number

import java.util.Scanner;

class Loop_2{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number: ");
		int num = sc.nextInt();

		System.out.println("All the even number from 1 to entered number: ");
		for(int i=1;i<=num;i++){
			if(i%2==0){
			    int d=i+1;
			    System.out.println(i);
			}
		}
	}
}
