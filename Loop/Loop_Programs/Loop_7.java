//WAP to read a number and check if it is a prime number or not

import java.util.Scanner;

class Loop_7{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int temp=0;

		for(int i = 2; i<num-1;i++){
			if(num%i==0){
				temp = temp +1;
			}
		}
		if(temp==0){
			System.out.println(num+" is Prime number");
		}
		else{
			System.out.println(num+" is not Prime number");
		}
	}
}
