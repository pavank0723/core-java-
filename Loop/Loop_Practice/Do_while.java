class Do_while{
	public static void main(String[] arg){
		int num = Integer.parseInt(arg[0]);
		
		if(num!=0){
			int i = 1;
			do{
				int sq = num * num;
				System.out.println(arg[0]+"² = "+sq);
				i++;
			}while(i<1);
		}
		else{
			System.out.println("Sorry you enter the 0 number");
		}
	}
}
