import java.util.*;

class SetDemo{
	public static void main(String[] arg){
		//Fastest sorting
		Set<String> weekdays = new HashSet<>();

		weekdays.add("Monday");
		weekdays.add("Tuesday");
		weekdays.add("Wedensday");
		weekdays.add("Thursday");
		weekdays.add("Friday");
		weekdays.add("Saturday");
		weekdays.add("Sunday");
		
		System.out.println("======== Hash set =======");

		for(String week:weekdays){
			System.out.println(week);
		}

		System.out.println("======After Sorting====");
		TreeSet <String> sorted = new TreeSet<>();
		sorted.addAll(weekdays);

		for(String ss: sorted){
			System.out.println(ss);
		}


		Set<String> weekdays1 = new LinkedHashSet<>();

                weekdays1.add("Monday");
                weekdays1.add("Tuesday");
                weekdays1.add("Wedensday");
                weekdays1.add("Thursday");
                weekdays1.add("Friday");
                weekdays1.add("Saturday");
                weekdays1.add("Sunday");

		System.out.println("======= Linked Hash set ========");
                for(String week:weekdays1){
                        System.out.println(week);
                }

		Set<Integer> num = new TreeSet<>();

                num.add(0);
                num.add(2);
                num.add(1);
                num.add(3);
                num.add(1);
                num.add(5);
                num.add(8);

                System.out.println("======= Tree set ========");
                for(int n:num){
                        System.out.println(n);
                }
	}
}
