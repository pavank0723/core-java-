import java.util.*;



class ComparatorDemo{
	public static void main(String[] arg){
		List<Integer> values = new ArrayList<Integer>();

		values.add(345);
		values.add(436);
		values.add(654);
		values.add(342);
		values.add(675);
		values.add(123);
		values.add(231);

		Collections.sort(values,(o1,o2) ->
		{
			return o1%10>o2%10?1:-1;		//turnary operator
				
		});

		for(int i: values){
			System.out.println(i);
		}

	}
}
