import java.util.*;

class Student implements Comparable {
	String name,city;
	int age,rollno;

	
	public int compareTo(Object o){
		Student s = (Student) o;
		if(this.rollno>s.rollno)
			return 1;
		else
			return -1;
	}

	//========-------Contractor-------============
	public Student(String n1,int a1, String c1,int r1 ){
		this.name = n1;
		this.age = a1;
		this.city = c1;
		this.rollno = r1;
	}

/*
	//Name
	public String getName(){
		return this.name;
	}

	public String setName(String n){
		this.name = n;
		return this.name;
	}

	//Age
	public int getAge(){
		return this.age;
	}

	public int setAge(int a){
		this.age = a;
		return this.age;
	}

	//City
	public String getCity(){
		return this.city;
	}

	public String setCity(String c){
		this.city = c;
		return this.city;
	}
*/

	public String toString() {
		return "Name: " + this.name + "|      --Age: " + this.age + "|       ---City: " + this.city + "|       ----Roll No: " +this.rollno;
	}
}

class StudentMain2{
	public static void main(String[] arg){
		Student sobj1 = new Student("Satyansh",18,"Mumbai  ",120);
		Student sobj2 = new Student("Pavan   ",20,"Kalyan  ",31);
		Student sobj3 = new Student("Chirag  ",14,"Bhusaval",118);
		List<Student> student = new ArrayList<>();

		student.add(sobj1);
		student.add(sobj2);
		student.add(sobj3);

		student.forEach(System.out::println);
		Collections.sort(student);

                 System.out.println("======------ Anonymouse Class  ------======");
                 Collections.sort(student, new NameComparator(){
                        public int compare(Object o3,Object o4){
                                Student s1 = (Student) o3;
                                Student s2 = (Student) o4;
                                return s1.name.compareTo(s2.name);
                        }
                });

		 System.out.println("======------ Name (Anonymouse Class) ------======");
		 for(Student obj:student){
		   System.out.println(obj);
		 }

		 Collections.sort(student, new Comparator(){
                        public int compare(Object o3,Object o4){
                                Student s1 = (Student) o3;
                                Student s2 = (Student) o4;
                                return s1.age - s2.age ;
                        }
                });
                 
		 System.out.println("======------ Age (Anonymouse Class) ------======");
                 for(Student obj:student){
                  System.out.println(obj);
                 }
		 Collections.sort(student, new CityComparator(){
                        public int compare(Object o3,Object o4){
                                Student s1 = (Student) o3;
                                Student s2 = (Student) o4;
                                return s1.city.compareTo(s2.city) ;
                        }
                });

                 System.out.println("======------ City (Anonymouse Class) ------======");
                 for(Student obj:student){
                  System.out.println(obj);
                 }

		 Collections.sort(student, new Comparator(){
                        public int compare(Object o3,Object o4){
                                Student s1 = (Student) o3;
                                Student s2 = (Student) o4;
                                return s1.rollno - s2.rollno ;
                        }
                });

                 System.out.println("======------ Roll No. (Anonymouse Class) ------======");
                 for(Student obj:student){
                  System.out.println(obj);
                 }

                 System.out.println("======------ Lambda Expresssion ------======");
		 Collections.sort(student,(o3,o4) ->{
                                Student s1 = (Student) o3;
                                Student s2 = (Student) o4;
                                return s1.name.compareTo(s2.name);
                        }
                 );

                 System.out.println("======------ Name (Lambda Expression) ------======");
                 for(Student obj:student){
                   System.out.println(obj);
                 }
	}
}
