import java.util.*;
class ListInterface{
	public static void main(String[] arg){
//		Collection<Integer> fibonacci = new ArrayList<>();
		List<Integer> fibonacci = new ArrayList<>();

		fibonacci.add(0);
		fibonacci.add(1);
		fibonacci.add(3);
		fibonacci.add(2);
		fibonacci.add(8);
		//fibonacci.add(3);
		fibonacci.add(4);
		fibonacci.add(6);

		fibonacci.remove(fibonacci.indexOf(6));

		System.out.println("Before sorting");
		
		fibonacci.forEach(System.out::println);
		
		Collections.sort(fibonacci);
		System.out.println("After sorting");			

		for(int i=0;i<fibonacci.size();i++){				
			System.out.println(i);					
		}
		
		System.out.println("Size of the List is:"+fibonacci.size());
	}
}
