import java.util.*;
class ListInterface4{
	public static void main(String[] arg){
//		Collection<Integer> fibonacci = new ArrayList<>();
		Stack<Integer> fibonacci = new Stack<>();
		
		fibonacci.push(0);
		fibonacci.push(1);
		fibonacci.push(3);
		fibonacci.push(2);
		fibonacci.push(8);
		//fibonacci.add(3);
		fibonacci.push(4);
		fibonacci.push(6);
		System.out.println("====---Stack---====");

		System.out.println("Before Poping");
		fibonacci.forEach(System.out::println);

		fibonacci.pop();
		System.out.println("After Poping");
		fibonacci.forEach(System.out::println);

		System.out.println("Before sorting");
		
		fibonacci.forEach(System.out::println);
			
		Collections.sort(fibonacci);
                System.out.println("After sorting");

                for(int i=0;i<fibonacci.size();i++){
                        System.out.println(i);
                }

                System.out.println("Size of the List is:"+fibonacci.size());
	}
}
