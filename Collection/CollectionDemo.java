import java.util.*;
class CollectionDemo{
	public static void main(String[] arg){
		Collection fruits = new ArrayList();
		fruits.add("Mango");
		fruits.add("Banana");
		fruits.add("Apple");
		fruits.add("Orange");

		for(Object i:fruits){
			System.out.println(i);
		}
//		System.out.println("Fruits (--ArrayList--) =>"+fruits);

		Collection weekdays = new HashSet();
		weekdays.add("Monday");
		weekdays.add("Tuesday");
		weekdays.add("Wednesday");
		weekdays.add("Thursday");
		weekdays.add("Friday");
		weekdays.add("Saturday");
		weekdays.add("Sunday");
		weekdays.add("Monday");
		weekdays.add("Tuesday");

		System.out.println("=========");
		weekdays.forEach((i)->{
			System.out.println(i);
		});

//		System.out.println("Week Days(--HashSet--) =>"+weekdays);

		Map fruits_map = new HashMap();

		fruits_map.put("Nagpur","Orange");
		fruits_map.put("Ratnagiri","Mangoes");
		fruits_map.put("Jalgav","Banana");
		fruits_map.put("Nashik","Graps");
		
//		System.out.println("Fruits Map (--Hash Map--) => "+fruits_map);
	}
}
