import java.util.*;

class AgeComparator implements Comparator {
	
	//Compare ===--- age ---====
	public int compare(Object o1, Object o2) {
		Student s1 = (Student) o1;
		Student s2 = (Student) o2;
		return s1.age > s2.age ? 1 : -1;
	}
	
}

class NameComparator implements Comparator{
//Compare ===--- name ---====   
        public int compare(Object o3,Object o4)     {
                Student s1 = (Student) o3;
                Student s2 = (Student) o4;
                return s1.name.compareTo(s2.name);
        }

}
class CityComparator implements Comparator{
//Compare ===--- city ---====
        public int compare(Object o5,Object o6) {
                Student s1 = (Student) o5;
                Student s2 = (Student) o6;
                return s1.city.compareTo(s2.city);
        }
}
class Student implements Comparable {
	String name,city;
	int age,rollno;

	
	public int compareTo(Object o){
		Student s = (Student) o;
		if(this.rollno>s.rollno)
			return 1;
		else
			return -1;
	}
/*	public int compareTo(Object o){

		Student s = (Student) o;  //Type cast 
		return this.name.compareTo(s.name);
	}*/

	//========-------Contractor-------============
	public Student(String n1,int a1, String c1,int r1 ){
		this.name = n1;
		this.age = a1;
		this.city = c1;
		this.rollno = r1;
	}

/*
	//Name
	public String getName(){
		return this.name;
	}

	public String setName(String n){
		this.name = n;
		return this.name;
	}

	//Age
	public int getAge(){
		return this.age;
	}

	public int setAge(int a){
		this.age = a;
		return this.age;
	}

	//City
	public String getCity(){
		return this.city;
	}

	public String setCity(String c){
		this.city = c;
		return this.city;
	}
*/

	public String toString() {
		return "Name: " + this.name + "|      --Age: " + this.age + "|       ---City: " + this.city + "|       ----Roll No: " +this.rollno;
	}
}

class StudentMain{
	public static void main(String[] arg){
		Student sobj1 = new Student("Satyansh",18,"Mumbai ",120);
		Student sobj2 = new Student("Pavan   ",20,"Kalyan ",31);
		Student sobj3 = new Student("Chirag  ",14,"Bhusaval",118);
		List<Student> student = new ArrayList<>();

		student.add(sobj1);
		student.add(sobj2);
		student.add(sobj3);

		student.forEach(System.out::println);
		Collections.sort(student);
		System.out.println("\n=============---------\nAfter Sorting Roll No\n=============---------\n");
		student.forEach(System.out::println);
		Collections.sort(student, new NameComparator());
		System.out.println("\n=============------\nAfter Sorting Name\n=============------\n");
		student.forEach(System.out::println);

		Collections.sort(student, new AgeComparator());
                System.out.println("\n============------\nAfter Sorting Age\n============------\n");
                student.forEach(System.out::println);

		Collections.sort(student, new CityComparator());
                System.out.println("\n=============-------\nAfter Sorting City\n=============-------\n");
                student.forEach(System.out::println);


	}
}
