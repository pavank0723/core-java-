import java.util.*;

//Sorting with last digit
//Inner Class (Anonymous Class)

class ComparatorDemo 2{
	public static void main(String[] arg){
		List<Integer> values = new ArrayList<Integer>();

		values.add(345);
		values.add(436);
		values.add(654);
		values.add(342);
		values.add(675);
		values.add(123);
		values.add(231);

		Comparator<Integer> com = new CompImp(){
			public int compare(Integer o1,Integer o2){
				if(o1%10 > o2%10)
					return 1;
				return -1;
			}
		};
		Collections.sort(values,com);

		for(int i: values){
			System.out.println(i);
		}

	}
}
