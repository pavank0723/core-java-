import java.util.*;
class MapDemo{
	public static void main(String[] arg){
		Map fruits = new HashMap();
	//Multiple null (key & value) is accepted in HashMap
		System.out.println("----------Hash Map=======");
		fruits.put("Nagpur","Orange");
		fruits.put("Jalgaon","Banana");
		fruits.put("Nashik","Graps");
		fruits.put("Ratnagiri","Mangoes");
		fruits.put(null,"Potato");

		Set fset = fruits.entrySet();

		Iterator i = fset.iterator();
		while(i.hasNext()){
			Map.Entry e = (Map.Entry)i.next();
			System.out.println(e.getKey()+" "+e.getValue());
		}
		System.out.println("Nashik Fruits "+fruits.get("Nashik"));
		
		Map fruit1 = new LinkedHashMap();

		System.out.println("----------Linked Hash Map=======");
                fruit1.put("Nagpur","Orange");
                fruit1.put("Jalgaon","Banana");
                fruit1.put("Nashik","Graps");
                fruit1.put("Ratnagiri","Mangoes");
                fruit1.put(null,"Potato");

                Set fset1 = fruit1.entrySet();

                Iterator i1 = fset1.iterator();
                while(i1.hasNext()){
                        Map.Entry e1 = (Map.Entry)i1.next();
                        System.out.println(e1.getKey()+" "+e1.getValue());
                }
                System.out.println(fruit1.get("Graps")+" Fruit "+fruit1.get("Nashik"));
		Map fruit2 = new TreeMap();

                System.out.println("----------Tree Map=======");
                fruit2.put("Nagpur","Orange");
                fruit2.put("Jalgaon","Banana");
                fruit2.put("Nashik","Graps");
                fruit2.put("Ratnagiri","Mangoes");
                fruit2.put("Null","Potato");

                Set fset2 =  fruit2.entrySet();

                Iterator i2 = fset2.iterator();
                System.out.println("All Keys");
                while(i2.hasNext()){
                        Map.Entry e2 = (Map.Entry)i2.next();
                        System.out.println(e2.getKey()+" ");
                }
                System.out.println("Nashik Fruits "+fruit2.get("Nashik"));

		System.out.println("---------------------");
		Hashtable fruit3 = new Hashtable();

                System.out.println("----------Hash Table=======");
                fruit3.put("Nagpur","Orange");
                fruit3.put("Jalgaon","Banana");
                fruit3.put("Nashik","Graps");
                fruit3.put("Ratnagiri","Mangoes");
                fruit3.put("Null","Potato");

                Set fset3 = fruit3.entrySet();

                Iterator i3 = fset3.iterator();
                while(i3.hasNext()){
                        Map.Entry e3 = (Map.Entry)i3.next();
                        System.out.println(e3.getKey()+" "+e3.getValue());
                }
                System.out.println("Nashik Fruits "+fruit3.get("Nashik"));
	}	
}
