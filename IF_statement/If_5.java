//WAP to read marks for 5 subjects and check if student is passed or not

import java.util.Scanner;
class If_5{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the marks of the 5 subject: ");

		System.out.println("Subject 1:");
		int sub1 = sc.nextInt();

		 System.out.println("Subject 2:");
 		 int sub2 = sc.nextInt();	

		 System.out.println("Subject 3:");
		 int sub3 = sc.nextInt();

		 System.out.println("Subject 4:");
		 int sub4 = sc.nextInt();

		 System.out.println("Subject 5:");
		 int sub5 = sc.nextInt();

		 int sum = sub1 + sub2 + sub3 + sub4 + sub5;

		 int percentage = sum/5;

		 if (percentage >= 40){
		 	System.out.println("Congratulation you are passed successfully: "+percentage+"%");
		 }
		 else{
		 	System.out.println("Sorry you are failed: "+percentage+"%");
		 }
	}
}
