//WAP to read 3 angles and check if triangle can be formed or not °
import java.util.Scanner;
class If_3{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the 1st angle: ");
		int a1 = sc.nextInt();

		System.out.print("Enter the 2nd angle: ");
        	int a2 = sc.nextInt();

		System.out.print("Enter the 3rd angle: ");
	        int a3 = sc.nextInt();

		int total = a1 + a2 + a3;

		System.out.println("Above angles are: "+a1+"° "+a2+"° "+a3+"° = "+total+"°");
		if(total == 180){
			System.out.println(">>Triangle can be formed,"+total+" = 180°");
		}
		else{
			if(total > 180){
				 System.out.println(">>Triangle can not be formed,"+total+"° > 180°");
			}
			else{
				System.out.println(">>Triangle can not be formed,"+total+"° < 180°");
			}
		}


	}
} 
