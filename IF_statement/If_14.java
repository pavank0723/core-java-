/*WAP to read a Salary of Employee and print commission according to following Criteria
	SAL               Commision
	<10000             10%
	10000-20000   	   12%
	>20000             15%
 
*/

import java.util.Scanner;

class If_14{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the salary of Employee: ₹");
		float sal = sc.nextFloat();

		if(sal <= 10000){
			float comm = (sal / 100)*10;
			System.out.print("\nSalary commission: ₹"+comm);
			float total = sal + comm;
			System.out.print("\nAfter commission salary is: ₹"+total);
		}
		else if((sal >=10000) && (sal <= 20000)){
			float comm = (sal / 100)*12;
		        System.out.print("\nSalary commission: ₹"+comm);
			float total = sal + comm;
                        System.out.print("\nAfter commission salary is: ₹"+total);
		}
		else if(sal >= 20000){
			float comm = (sal / 100)*15;
		        System.out.print("\nSalary commission: ₹"+comm);
			float total = sal + comm;
                        System.out.print("\nAfter commission salary is: ₹"+total);
		}
		else{
			System.out.println("\nNo commision");
		}
	}
}
