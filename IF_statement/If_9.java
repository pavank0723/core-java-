//WAP to read 3 numbers and check their last digit is same or not

import java.util.Scanner;

class If_9{
        public static void main(String[] arg){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the 1st number:");
                int num1 = sc.nextInt();

                System.out.println("Enter the 2nd number:");
                int num2 = sc.nextInt();

		System.out.println("Enter the 3rd number:");
                int num3 = sc.nextInt();

                if((num1%10 == num2%10) && (num2%10 == num3%10) && (num1%10 == num3%10)){
                        System.out.println("Last digit of all three number is same: "+num1+" "+num2+" "+num3);
                }
                else{
                        System.out.println("Last digit of both number is not same: "+num1+" "+num2+" "+num3);
                }

        }
}

