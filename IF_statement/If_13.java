//WAP to read a year and check if it is Leap year or not

import java.util.Scanner;

class If_13{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the year:");
		int year = sc.nextInt();

		if(year%4 == 0){
			if(year%100 == 0){
				if(year%400 == 0){
					System.out.println("It is leap year "+year);
				}
				else{
					System.out.println("It is not leap year "+year);
				}
			}
			else{
				 System.out.println("It is leap year "+year);
			}
		}
		else{
			 System.out.println("It is not leap year "+year);
		}
	}
}
