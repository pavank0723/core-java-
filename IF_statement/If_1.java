//WAP to read a number and check if it is even or odd
import java.util.Scanner;

class If_1{
	public static void main(String[] arg){
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter the number: ");
		int num = sc.nextInt();


		if(num%2 == 0){
			System.out.println(num+" is even number");
		}
		else{
			System.out.println(num+" is odd number");
		}

	}
}
