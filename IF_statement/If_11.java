//WAP to read 3 angles and check if triangle can be formed or not. If triangle can be formed then check it it is equilateral,isosceles or right angled triangle

import java.util.Scanner;
class If_11{
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);

                System.out.print("Enter the 1st angle: ");
                int a1 = sc.nextInt();

                System.out.print("Enter the 2nd angle: ");
                int a2 = sc.nextInt();

                System.out.print("Enter the 3rd angle: ");
                int a3 = sc.nextInt();

                int total = a1 + a2 + a3;

                System.out.println("Above angles are: "+a1+"° "+a2+"° "+a3+"° = "+total+"°");
		 if(a1!=a2 || a2!=a3){
                      System.out.println("Some angle is less than 0° or equal to 0°");
                 }

		 else if(a1 >0 && a2 >0 && a3 >0){
		    if(total == 180 && a1==a2 && a2==a3){
         		    System.out.println("All angles are greater than 0°");
			    System.out.println(">>Triangle can be formed and this is an equilateral triangle "+total+"° = 180°");
		    }
		 }
		 else if(total == 180 && (a1 == a2)||(a2 == a3) || (a1 == a3)){
		    	    System.out.println("All angles are greater than 0°");
                            System.out.println(">>Triangle can be formed and this is an isosceles triangle "+total+"° = 180°");
		 }               
	        
		
                 else{
			/*if(a1!=a2 && a2!=a3){
                                 System.out.println("Some angle is less than 0° or equal to 0°");
			}*/
			if(total > 180){
				System.out.println(">>Triangle can not be formed and not an equilateral triangle "+total+"° > 180°");
			}
			else{
                                System.out.println(">>Triangle can not be formed,"+total+"° < 180°");
                        }
                }


        }
}

