//WAP to read 2 numbers and check if their last digit is same of not
import java.util.Scanner;

class If_2{
	public static void main(String[] arg){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the 1st number:");
		int num1 = sc.nextInt();

		System.out.println("Enter the 2nd number:");
		int num2 = sc.nextInt();

		if(num1%10 == num2%10){
			System.out.println("Last digit of both number is same: "+num1+" "+num2);
		}
		else{
			System.out.println("Last digit of both number is not same: "+num1+" "+num2);
		}

	}
}
