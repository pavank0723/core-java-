/* WAP to read percentage of student and print Div
	>=75 : 1st class with distinction
	60-75: 1st class
	50-60: 2nd class
	40-50: 3rd class
	<40: fail
*/

import java.util.Scanner;
class If_15{
        public static void main(String[] arg){
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter the marks of the 5 subject: ");

                System.out.println("Subject 1:");
                int sub1 = sc.nextInt();

                 System.out.println("Subject 2:");
                 int sub2 = sc.nextInt();

                 System.out.println("Subject 3:");
                 int sub3 = sc.nextInt();

                 System.out.println("Subject 4:");
                 int sub4 = sc.nextInt();

                 System.out.println("Subject 5:");
                 int sub5 = sc.nextInt();

                 int sum = sub1 + sub2 + sub3 + sub4 + sub5;

                 int percentage = sum/5;

		 if((sub1 <0 || sub1 >100) || (sub2 <0 || sub2 >100) || (sub3 <0 || sub3 >100) || (sub4 <0 || sub4 >100) || (sub5 <0 || sub5 >100)){
		 	 System.out.println("Sorry you enter wrong marks");
		 }
		 else{
	                 if ((sub1 >=0 && sub1 >=40) && (sub2 >=0 && sub2 >=40) && (sub3 >=0 && sub3 >=40) && (sub4 >=0 && sub4 >=40) && (sub5 >=0 && sub5 >=40)){
				 if(sub1 <=100 && sub2 <=100 && sub3 <=100 && sub4 <=100 && sub5 <=100 && sum >=200){
					 System.out.println("Obtained  marks: "+sum+" Out of: 500");
					 if(percentage >=75){
						System.out.println("Congratulation you are passed successfully: "+percentage+"%");
				 	 }
					 else if(percentage >= 60 && percentage <= 75){
						System.out.println("Congratulation you are passed successfully: "+percentage+"% => First Class");
					 }
					 else if(percentage >= 50 && percentage <= 60){
					 	 System.out.println("Congratulation you are passed successfully: "+percentage+"% => Second Class");
					 }
					 else if(percentage >= 40  && percentage <= 50){
			                         System.out.println("Congratulation you are passed successfully: "+percentage+"% => Third Class");
                         		 }
				}
				 else {
	                               System.out.println("Obtained  marks: "+sum+" Out of: 500");           	               
                                       System.out.println("Sorry you are failed: "+percentage+"%");
                        	        
                          	 }

		              	
                  	   }
                  	   else if(sub1 < 40 || sub2 <40 || sub3 <40 || sub4 <40 || sub5 <40){
				  System.out.println("Obtained  marks: "+sum+" Out of: 500");
                                  System.out.println("Sorry you are failed: "+percentage+"%");
                            }    	  
       		  }
	}
}


