//WAP to read 2 numbers and find the greatest of them
import java.util.Scanner;

class If_6{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the 1st number:");
		int num1 = sc.nextInt();

		System.out.println("Enter the 2nd number:");
		int num2 = sc.nextInt();

		if(num1>num2){
			System.out.println("1st number is greater than 2nd number: "+num1+" == "+num1+" > "+num2);
		}
		else{
			System.out.println("2nd number is greater than 1st number: "+num2+" == "+num2+" > "+num1);
		}
		
	}
}
