import java.util.Scanner;

//<<<<<<======= Bombing Bank Program ======>>>>>>

class Bank{
	Scanner sc = new Scanner(System.in);
	int depo,with,balance=0;

	public Bank(int b){
		this.balance = b;
		System.out.println("======================\nAmount balance Rs."+this.balance+"\n======================");
	}

	public synchronized void deposit(int amount){

		System.out.println("-----------------+++++++\nDeposit: => |"+amount+"|\n-----------------+++++++");
		this.balance += amount;
		System.out.println("After deposit balance is "+this.balance);
		notify();

	}
	public synchronized void withdraw(int amount){
		System.out.println("\nWithdraw: => "+amount);

		
		if(this.balance <= amount){
			System.out.println("Insufficient balance");
			System.out.println("Current balance Rs."+this.balance);
			try{wait();}catch(Exception e){}
		}
			this.balance -= amount;
			System.out.println("After withdraw balance is "+this.balance);
			System.out.println("Current balance Rs."+this.balance);
	}
}
class MainBank{
	public static void main(String[] arg){
		Bank b1 = new Bank(500);
		Thread wt = new Thread(() -> {
			b1.withdraw(1000);		
		});

		Thread ds = new Thread(() -> {
			b1.deposit(1000);
		});		

		ds.start();
		wt.start();

		//b1.withdraw(200);
	}
}

