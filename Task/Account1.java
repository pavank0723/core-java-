import java.util.Scanner;

class RBI{
	Scanner sc = new Scanner(System.in);

	public float balance=0.0f, depo,with;

	public RBI(float b){
		this.balance = b;
		System.out.println("Account balance is: ₹"+this.balance);
	}

	public void deposit(){		
		System.out.print("Enter the amount to be deposit: ₹");
		depo = sc.nextFloat();
		this.balance = this.balance + depo;
	}
	public void withdraw(){		
		System.out.print("Enter the amount to be withdraw: ₹");
		with = sc.nextFloat();
		this.balance = this.balance - with;
	}

	public void showBalance(){		
		System.out.println("Current balance is: ₹"+this.balance);
	}
}

class SBI extends RBI{
	public SBI(float bal){
		super(bal);
	}
	public void deposit(){
                    System.out.print("Enter the amount to be deposit: ₹");
                    depo = sc.nextFloat();
                    this.balance = this.balance + depo;
        }

	public void withdraw(){
	    for(int count = 1; count <=5;count++){
		if(count<=3){
		    System.out.print("Enter the amount to be withdraw: ₹");
                    with = sc.nextFloat();
                    this.balance = this.balance - with;
		}
		else{
	            System.out.print("Enter the amount to be withdraw: ₹");
                    with = sc.nextFloat();
                    this.balance = (this.balance - with)-20;
		}
		count++;
	    }
	}


}

class MainRBI{
	public static void main(String[] arg){
		SBI acc1 = new SBI(100f);

		acc1.showBalance();
		acc1.deposit();
		acc1.showBalance();
		acc1.withdraw();
		acc1.showBalance();
	}	
}
