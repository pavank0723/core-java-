import java.util.Scanner;
interface MyDB{
	public void select();
}

class SQLdb implements MyDB{
	public void select(){
		System.out.println("I'm MySQL Select Method");
	}
}

class Oracledb implements MyDB{
	public void select(){
		System.out.println("I'm Oracle Select Method");	
	}
}

class DIClass{
	MyDB dobj;
	public DIClass(String db){
		switch(db){
			case "SQLdb":
				this.dobj = new SQLdb();
				break;
			case "Oracledb":
				this.dobj = new Oracledb();
				break;
			default:
				System.out.println("INVALID Database....");
				break;

		}
	}	
	public void select(){
		this.dobj.select();
	}
}

class FDPMain{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the DataBase name: ");
                String s = sc.nextLine();
		
		DIClass obj = new DIClass(s);
		obj.select();
	
	}
}
