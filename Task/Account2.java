import java.util.Scanner;

abstract class RBI{
	Scanner sc = new Scanner(System.in);
	protected int balance = 0,depo,with;

	abstract public void deposit();
	abstract public void withdraw();
	public void showBalance(){			
			System.out.println("================== ++++++++");
                        System.out.println("Current balance is: ₹"+this.balance);
			System.out.println("================== ++++++++");
        }	
        public void showMenu(){
                   Scanner sc1 = new Scanner(System.in);
                   int choice;

                   do{
                  	System.out.print("\n1. Deposit:\n2. Withdraw:\n3. Show balance:\n4. Exit: ");
                        System.out.print("\nEnter the choice you want to choose us: ");
                        choice = sc1.nextInt();
                        switch(choice){
                                        case 1:
                                                deposit();
                                                break;
                                        case 2:
                                                withdraw();
                                                break;
                                        case 3:
                                                showBalance();
                                                break;
					case 4:
                 				System.out.println("Thank you!!");
						break;
                                        default:
                                                System.out.println("Invalid");
						break;

                       }
                 }while(choice !=4);

        }		
	
}

class SBI extends RBI{
    	public SBI(int b){		
		this.balance = b;
		System.out.println("================== ++++++++");
		System.out.println("Amount balance is: ₹"+this.balance);
		System.out.println("================== ++++++++");
	}

	public void deposit(){
		System.out.print("Enter the amount to be deposit: ₹");
		depo = sc.nextInt();
		this.balance = this.balance + depo;
	}

	int count = 0;
	public void withdraw(){
			if(count<=2){
				System.out.print("Enter the amount to be withdraw: ₹");
				with = sc.nextInt();
				this.balance = this.balance - with;
			}
			else{
				System.out.print("Enter the amount to be withdraw: ₹");
                                with = sc.nextInt();
                                this.balance = (this.balance - with)-25;
			}
			count++;	
	}	
			   	
}

class HDFC extends RBI{
    	public HDFC(int b){		
		this.balance = b;
		System.out.print("Amount balance is: ₹"+this.balance);
	}

	public void deposit(){
		System.out.print("\nEnter the amount to be deposit: ₹");
		depo = sc.nextInt();
		this.balance = this.balance + depo;
	}

	int count = 0;
	public void withdraw(){
			if(count<=2){
				System.out.print("Enter the amount to be withdraw: ₹");
				with = sc.nextInt();
				this.balance = this.balance - with;
			}
			else{
				System.out.print("Enter the amount to be withdraw: ₹");
                                with = sc.nextInt();
                                this.balance = (this.balance - with)-20;
			}
			count++;	
	}
		
}

class MainRBI2{
	public static void main(String[] arg){
		Scanner sc1 = new Scanner(System.in);
		int accChoice;

		System.out.println("================================================");
		System.out.println("========++++-> WELCOME TO OUR BANK <-++++=======");
		System.out.println("================================================");
		System.out.println("1. SBI\n2. HDFC");
		System.out.print("Enter your bank no. to be choose: ");
		accChoice = sc1.nextInt();

		switch(accChoice){
			case 1:
				System.out.println("Welcome to SBI Bank");
				RBI acc1 = new SBI(500);
				acc1.showMenu();
				break;
			case 2:
				System.out.println("Welcome to HDFC Bank");
				RBI acc2 = new HDFC(200);
				acc2.showMenu();
				break;
			default:
				System.out.println("Invalid bank no. to be entered");
	       }
    	}
}
