import java.lang.Thread;

class Hi implements Runnable{
	@Override
	public void run(){
		for(int i=1;i<=1000;i++){
			System.out.println("Hi");
		}
	}
}

class HooKoo implements Runnable{
	@Override
	public void run(){
		for(int i=1;i<=1000;i++){
			System.out.println("HooKoo");
		}
	}
}

class MainRunable{
	public static void main(String[] arg){

		Thread h1 = new Thread(new Hi());
		Thread h2 = new Thread(new HooKoo());
		h1.start();
		h2.start();
		
	}
}
