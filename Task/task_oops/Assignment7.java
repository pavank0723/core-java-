import java.util.Scanner;
class Calculation{
//	int num1,num2,total;
	 public Calculation(){
	 	System.out.println("Please choose Calculation: ");
	 }
	 public void total(){
	 	System.out.println("This is main Calculation");
	 }
}

class Addition extends Calculation{
	int num1,num2,total;
	public Addition(int n1, int n2){
		this.num1 = n1;
		this.num2 = n2;		
	}
	public void total(){
		total = this.num1 + this.num2;
		System.out.print("1st num = "+this.num1+"\n2nd num = "+this.num2);
		System.out.println("\nTotal "+total);
	}
}

class Substraction extends Calculation{
	int num1,num2,total;
	public Substraction(int n1, int n2){
		this.num1 = n1;
		this.num2 = n2;
	}
	public void total(){
		total = this.num1 - this.num2;
		System.out.print("1st num = "+this.num1+"\n2nd num = "+this.num2);
		System.out.println("\nTotal "+total);
	}
}

class MainFactory{
	public static void main(String[] arg){
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter the Calculation you want to be calculate: ");
		int choice = sc.nextInt();
		Calculation c;
		do{
			switch(choice){
				case 1:
				c = new Addition(4,7);
				break;
	
				case 2:
				c = new Substraction(3,2);
				break;

				case 3:
				
				default:
				c = new Calculation();
				break;
				
			}
		}while(choice !=3);
		c.total();
	}
}
