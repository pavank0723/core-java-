//Inheritance with Abstract Class 

import java.util.Scanner;
abstract class Calculate{
	Scanner sc = new Scanner(System.in);
	public int num1, num2,total;
	//declare
	public abstract void total();

	//concrete method (declare & implement)
	public void accept(){
		System.out.print("1st number ");
		num1 = sc.nextInt();
		
		System.out.print("2nd number ");
                num2 = sc.nextInt();

	}
}

class Additon extends Calculate{
	//implementation
	public void total(){
		System.out.println("++++ Addition ++++");
		total = num1 + num2;		
		System.out.println("Total "+total);
	}
}

class MainAbstract{
	public static void main(String[] arg){
		Addition a1 = new Addition();
		a1.accept();
		a1.total();
	}
}
