//Assignment 4 -> Inheritance (Single Inheritance)

import java.util.Scanner;
class Calculate{
	Scanner sc = new Scanner(System.in);
	public int num1,num2,total;
	public Calculate(){
		System.out.print("1st number: ");
		num1 = sc.nextInt();

		System.out.print("2nd number: ");
        	num2 = sc.nextInt();
	}

}

class Addition extends Calculate{
	public void total(){
		total = num1 + num2;	
		System.out.println("Total "+total);
	}
}

class MainCalInheri{
	public static void main(String[] arg){
		Addition a1 = new Addition();
		a1.total();
	}
}
