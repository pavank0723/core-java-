
class MyDataBase{
	static MyDataBase obj; //= new MyDataBase();
	private MyDataBase(){
		System.out.println("MyDataBase object initialized.....");
	}

	public static MyDataBase getConnectionObject(){
		System.out.println("Connection Object returned");
		synchronized(MyDataBase.class){
			if(obj == null){
				obj =  new MyDataBase();	
			}
		}
		return obj;
	}
}

class MainDB2{
	public static void main(String[] arg){
		Thread t1 = new Thread(() -> {			
			MyDataBase db1 = MyDataBase.getConnectionObject(); //
		});
		Thread t2 = new Thread(() -> {
			MyDataBase db2 = MyDataBase.getConnectionObject(); //		
		});

		t1.start();
		t2.start();
	}
}
