//import java.util.Scanner;
class RBI{
	private float balance,sb = 0.0f;

	RBI(float b){
		this.balance = b;
		System.out.println("Your balance is: "+this.balance);
		sb=this.balance;
	}

	public float deposite(float depo){
		System.out.println("Enter amount to be deposited: "+depo);
		sb = this.balance + depo;
		return sb;
	}

	public float withdraw(float with){
		System.out.println("Enter amount to be withdraw: "+with);
		sb = this.balance + with;
//		this.balance += with;
		return sb;	
	}

	public void showBalance(){
		System.out.println("Current balance is ₹ "+sb);
		
	}
}

class MainRBI{
	public static void main(String[] arg){
		//Scanner sc = new Scanner(System.in);

		RBI acc1 = new RBI(500f);

		acc1.showBalance();
		acc1.deposite(1000f);
		acc1.showBalance();
		acc1.withdraw(500f);		
		acc1.showBalance();

	}
}


