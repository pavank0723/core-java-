
class MyDataBase{
	static MyDataBase obj = new MyDataBase();
	private MyDataBase(){
		System.out.println("MyDataBase object initialized.....");
	}

	public static MyDataBase getConnectionObject(){
		System.out.println("Connection Object returned");	
		return obj;
	}
}

class MainDB{
	public static void main(String[] arg){
		MyDataBase db1 = MyDataBase.getConnectionObject();
		MyDataBase db2 = MyDataBase.getConnectionObject();
	}
}
