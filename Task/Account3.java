
interface RBI{

	public void deposit(int amount);
	public void withdraw(int amount);
	

}

class SBI implements RBI{

	int balance=0;

	public void deposit(int amount){
		this.balance +=amount;
		System.out.println("deposit "+this.balance);
	}

	public void withdraw(int amount){
		this.balance -=amount;
		System.out.println("withdraw "+this.balance);
	}
}

class MainRBI2{
	public static void main(String[] ar){
		SBI acc1 = new SBI();
		acc1.deposit(200);
		acc1.withdraw(50);
		
	}
}
