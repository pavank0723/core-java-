import java.io.*;

// Read Data from File
class TestDemo2{
	public static void main(String[] arg) throws Exception{
		FileReader r = new FileReader("ReaderInput.txt");

		int i;
		while((i=r.read())!=-1){
			System.out.print((char)i);
		}
		r.close();
	}
}


