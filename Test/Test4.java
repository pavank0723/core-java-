//Read data from file Student data

import java.io.*;

class TestDemo4{
	public static void main(String[] arg){
		try{
			FileInputStream fis = new FileInputStream("StudentObject.txt");

			ObjectInputStream ois = new ObjectInputStream(fis);

			Student so1 = (Student)ois.readObject();
			Student so2 = (Student)ois.readObject();
			Student so3 = (Student)ois.readObject();
			Student so4 = (Student)ois.readObject();
			System.out.println("Done");
			// closing resources
			ois.close();
			fis.close();

			System.out.println(so1.toString());
			System.out.println(so2.toString());
			System.out.println(so3.toString());
			System.out.println(so4.toString());
		}

		catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
}
