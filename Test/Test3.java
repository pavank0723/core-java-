//Save Object data (Student) in file

import java.io.*;

class Student implements Serializable{
	String name,addr;
	int age,roll_no;

	public Student(String n,String c,int a, int r){
		this.name = n;
		this.addr = c;
		this.age = a;
		this.roll_no = r;
	}
	@Override
	public String toString(){
		return "Student:: Name ="+this.name+"| Address ="+this.addr+"| Age ="+this.age+"| Roll No ="+this.roll_no;
	}
}

class TestDemo3{
	public static void main(String[] arg){
		Student s1 = new Student("Satyansh","Sahad   ",19,12);
		Student s2 = new Student("Chirag  ","Vikhroli",20,53);
		Student s3 = new Student("Vikas   ","Kurla   ",18,23);
		Student s4 = new Student("Bhavesh ","Bhandup ",21,64);

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);

		try{
			FileOutputStream fos = new FileOutputStream("StudentObject.txt");

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(s1);
			oos.writeObject(s2);
			oos.writeObject(s3);
			oos.writeObject(s4);

			System.out.println("Done");
			// closing resources
			oos.close();
			fos.close();
		}

		catch(Exception e){
			e.printStackTrace();
		}
	}
}
