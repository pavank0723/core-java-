
#include<stdio.h>

void swap(int *a, int *b){
	*a = *a + *b;
	*b = *a - *b;
	*a = *a - *b;
}

/*void swap(int a, int b){
	a = a + b;
        b = a - b;
        a = a - b;
}*/
int main(){
	int x = 50;
	int y = 40;

	printf("Before Swapping");
	printf("\nvalue of x is %d",x);
	printf("\nvalue of x is %d",y);

	swap(&x,&y); 	//Call by refrence
//	swap(x,y); 	//Call by value

	printf("\nAfter Swapping");
        printf("\nvalue of x is %d",x);
        printf("\nvalue of x is %d",y);
}

