
class Student{
	private String name,city;
	private int age;
	Student(String n,String c, int a){
		this.name = n;
		this.city = c;
		this.age = a;
	}
	
	public int getAge(){
		return this.age;
	
	}
}

class MainArray{
	public static void main(String[] arg){
		Student s1 = new Student("Student1","City1",23);
		Student s2 = new Student("Student2","City2",33);
		Student s3 = new Student("Student3","City3",43);

		Student arrayObj[] = {s1,s2,s3};   //Student datatype

		int total = 0;
		for(int i=0;i<arrayObj.length;i++){
			total += arrayObj[i].getAge();
		}
		int avg = total/arrayObj.length;

		System.out.println(avg);
	}
}
