/* WAP to read a color code (char value) and print appropriate color.
   (e.g. R – Red, G- Green, B-Blue and other char – Black)*/

import java.util.Scanner;
class Switch_4{
	public static void main(String[] arg){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the code: ");
		char code = sc.next().charAt(0);
	
		String color;
		switch(code){
		
		 case 'R':
			  color = "Red";
			  System.out.println("This is "+color);
			  break;
		 case 'G':
			  color = "Green";
  		      	  System.out.println("This is "+color);
			  break;
		 case 'B':
                          color = "Blue";
                          System.out.println("This is "+color);
                          break;

		}
	}
}


